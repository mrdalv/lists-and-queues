/*Связанные списки отличаются от массива тем, что под каждый выделяется
динамическая память. Каждый элемент состоит из двух частей. Адресная часть и сам
элемент. Каждый элемент представляет собой структуру, состоящую из несколькоких
частей. В адресной части в
однонаправленном списке находится адрес следующего, в двунаправленном ещё адрес
предыдущего.*/

#include <conio.h>
#include <dos.h>
#include <stdio.h>

#include <cstdlib>
#include <iomanip>
#include <iostream>

using namespace std;
//Однонаправленный линейный список
void OneList() {
    struct list {
        int a;
        list *next;
    } * first, *p;

    first = new list;
    int j;

    cout << "Введите количество элементов-> ";
    cin >> j;
    cout << "Заполните элементами:" << endl;
    cin >> first->a;

    first->next = NULL;
    p = first;

    for (int i = 0; i < j; i++) {
        p->next = new list;
        if (i == j - 1)
            p->next->a = 0;
        else
            cin >> p->next->a;
        p->next->next = NULL;
        p = p->next;
    }

    p = first;

    for (int i = 0; i < j; i++) {
        cout << p->a << " ";
        p = p->next;
    }

    cout << endl;

    delete (first);
    delete (p);
    system("pause");
}
//Однонаправленный кольцевой список
void TwoList() {
    struct list {
        int a;
        list *next;
    } * first, *p;

    first = new list;
    int j;

    cout << "Введите количество элементов-> ";
    cin >> j;
    cout << "Заполните элементами:" << endl;
    cin >> first->a;

    first->next = NULL;
    p = first;

    for (int i = 0; i < j; i++) {
        p->next = new list;
        if (i == j - 1)
            p->next->a = 0;
        else
            cin >> p->next->a;
        p->next->next = NULL;
        p = p->next;
    }

    p->next = first;
    p = first;

    for (int i = 0; i < j; i++) {
        cout << p->a << " ";
        p = p->next;
    }

    cout << endl;

    delete (first);
    delete (p);
    system("pause");
}
//Двунаправленный линейный список
void ThreeList() {
    struct list {
        int a;
        list *next, *pred;
    } * first, *p;

    first = new list;
    int j;

    cout << "Введите количество элементов-> ";
    cin >> j;
    cout << "Заполните элементами:" << endl;
    cin >> first->a;

    first->next = NULL;
    first->pred = NULL;
    p = first;
    for (int i = 0; i < j; i++) {
        p->next = new list;
        if (i == j - 1)
            p->next->a = 0;
        else
            cin >> p->next->a;
        p->next->next = NULL;
        p->next->pred = p;
        p = p->next;
    }

    p = p->pred;

    for (int i = j; i > 0; i--) {
        cout << p->a << " ";
        p = p->pred;
    }

    cout << endl;

    delete (first);
    delete (p);
    system("pause");
}
//Двунаправленный кольцевой список
void FourList() {
    struct list {
        int a;
        list *next, *pred;
    } * first, *p;
    first = new list;
    int j;

    cout << "Введите количество элементов-> ";
    cin >> j;
    cout << "Заполните элементами:" << endl;
    cin >> first->a;
    first->next = NULL;
    first->pred = NULL;
    p = first;
    for (int i = 0; i < j; i++) {
        p->next = new list;
        if (i == j - 1)
            p->next->a = 0;
        else
            cin >> p->next->a;
        p->next->next = NULL;
        p->next->pred = p;
        p = p->next;
    }
    p = p->pred;
    first->pred = p;
    for (int i = j; i > 0; i--) {
        cout << p->a << " ";
        p = p->pred;
    }
    cout << endl;
    delete (first);
    delete (p);
    system("pause");
}
//Стэк
int stack[10];
int in_stack = 0;
void push(int x) {
    stack[in_stack] = x;
    in_stack++;
}
int pop() {
    if (in_stack == 0) {
        cout << "c??? ???? \n";
        return (-1);
    }
    in_stack--;
    return stack[in_stack];
}
void Stack() {
    push(1);
    push(2);
    push(3);

    while (in_stack > 0) cout << "\n ???????=" << pop() << endl;
    getch();
}
//Сортировка связанного списка
struct List {
    int x;
    List *next;
} * first, *p, *q, *imin;

void Sortirovka() {
    int j, i;
    for (j = 0; j < 4; j++, p = p->next) {
        imin = p;
        for (i = j + 1, q = p->next; i < 5; i++, q = q->next) {
            if (q->x < imin->x) {
                imin = q;
            }
        }
        int min;
        min = imin->x;
        imin->x = p->x;
        p->x = min;
    }
}
void StackSort() {
    int n;
    cout << "Введите количество элементов-> ";
    cin >> n;
    cout << "Заполните элементами:" << endl;
    int i;
    first = new List;
    cin >> first->x;
    first->next = NULL;
    p = first;
    for (i = 1; i < n; i++) {
        p->next = new List;
        cin >> p->next->x;
        p->next->next = NULL;
        p = p->next;
    }
    p = first;
    Sortirovka();
    p = first;
    cout << "Отсортированный массив: ";
    for (i = 1; i < n + 1; i++) {
        cout << p->x << " ";
        p = p->next;
    }
    cout << endl;
    system("pause");
}
//Реализация очереди на базе массива
struct steck  //Определение стека
{
    int x;        //Значение
    steck *pred;  //адрес предыдущего элемента
};
// steck* top,*p;
steck *first2(int x);  //Объявление функции  заполнения 1-ого элемента стека
void push(steck **top, int x);  //Объявление функции  заполнения стека
int pop(
    steck **top);  //Объявление функции  выборки и удаления элемента из стека
void steck_spisoc() {
    steck *top = first2(1);
    for (int i = 2; i < 6; i++) push(&top, i);
    while (top) cout << setw(4) << pop(&top) << endl;
    system("pause");
}
steck *first2(int x)  //Определение функции  заполнения 1-ого элемента стека
{
    steck *p =
        new steck;  // Выделение динамической памяти под 1-ый элемент стека
    p->x = x;       //Присвоение значения
    p->pred =
        0;  //В адрес предыдущего заносится 0,т.к. предыдущего элемента нет
    return p;  //Возвращается адрес текущего элемента
}
void push(steck **top, int x)  // Определение функции  заполнения стека
{
    steck *p = new steck;
    p->x = x;
    p->pred = *top;
    *top = p;
}
int pop(
    steck **top)  //  Определение функции  выборки и удаления элемента из стека
{
    int temp = (*top)->x;  //Сохранение значения
    steck *p = *top;  //Сохранение текущего указателя
    *top = (*top)->pred;  //Предыдущий элемент делаем текущим
    delete (p);
    return (temp);
}
//Стэк через список
struct queue  //Определение очереди
{
    int x;        //Значение
    queue *next;  //адрес следующего элемента
};
queue *first1(int x);  //Объявление функции  заполнения 1-ого элемента очереди
void push(queue **top, int x);  //Объявление функции  заполнения очереди
int pop(
    queue **top);  //Объявление функции  выборки и удаления элемента из  очереди
void queue_spisok() {
    queue *pbeg = first1(1);
    queue *pend = pbeg;
    for (int i = 2; i < 6; i++) push(&pend, i);
    while (pbeg) cout << setw(4) << pop(&pbeg) << endl;
    system("pause");
}
queue *first1(int x)  //Определение функции  заполнения 1-ого элемента очереди
{
    queue *p =
        new queue;  // Выделение динамической памяти под 1-ый элемент очереди
    p->x = x;       //Присвоение значения
    p->next = 0;  //В адрес следующего элемента заносится 0,т.к. следующего
                  //элемента нет
    return p;  //Возвращается адрес текущего элемента
}
void push(queue **pend, int x)  // Определение функции  заполнения  очереди
{
    queue *p = new queue;
    p->x = x;
    p->next = 0;
    (*pend)->next = p;
    *pend = p;
}
int pop(queue **pbeg)  //  Определение функции  выборки и удаления элемента из
                       //  очереди
{
    int temp = (*pbeg)->x;  //Сохранение значения
    queue *p = *pbeg;  //Сохранение текущего указателя
    *pbeg = (*pbeg)->next;  //Следующий элемент делаем текущим
    delete (p);
    return (temp);
}
//Основная программа
int main() {
    setlocale(0, "");
    int n;

    while (n != 9) {
        cout << "Списки:" << endl;
        cout << "1) Однонаправленный - Линейный" << endl;
        cout << "2) Однонаправленный - Кольцевой" << endl;
        cout << "3) Двунаправленный - Линейный" << endl;
        cout << "4) Двунаправленный - Кольцевой" << endl;
        cout << "5) Стэк" << endl;
        cout << "6) Сортировка связанного списка" << endl;
        cout << "7) Список через очередь" << endl;
        cout << "8) Стэк в списке" << endl;
        cout << "9) Конец программы" << endl;
        cin >> n;

        switch (n) {
            case 1: {
                OneList();
                break;
            }
            case 2: {
                TwoList();
                break;
            }
            case 3: {
                ThreeList();
                break;
            }
            case 4: {
                FourList();
                break;
            }
            case 5: {
                Stack();
                break;
            }
            case 6: {
                StackSort();
                break;
            }
            case 7: {
                queue_spisok();
                break;
            }
            case 8: {
                steck_spisoc();
                break;
            }
            case 9: {
                cout << "Выход из программы!";
                break;
            }
            default: {
                cout << "Введите корректный номер операции!";
                break;
            }
        }
    }
    delete (first);
    delete (p);
    delete (q);
    delete (imin);
    return 0;
}
